## Test

In order to do a test of this API try to send this body using POSTMAN or other tool.

<b>POST</b>: http://localhost:8080/api/submit

```
{
"email": "sam@email.com",
"name": "Sam",
"phone": "221-442-3542",
"pickup": "ASAP",
"subtotal": 73.32,
"tax": 5.8656,
"total": 79.1856,
"orderItem": [{"section": "Lo Mein", "name": "Beef Lo Mein", "size": "Small", "price": 2}]
}
```

The main problem related with the persistence is that in the orderItem the Angular send the "id" so this produce some errors. I use a H2 which is a database in memory just for testing proposes.
