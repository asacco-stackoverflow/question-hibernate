package com.stackoverflow.question.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackoverflow.question.entity.SubmitOrder;
import com.stackoverflow.question.repository.QuestionRepository;

@RestController
@RequestMapping("/api")
public class QuestionController {
    private QuestionRepository questionRepository;

    @Autowired
    public QuestionController(QuestionRepository questionRepository) {
    	this.questionRepository = questionRepository;
    }
    
    @PostMapping("/submit")
    public SubmitOrder saveOrder(@RequestBody SubmitOrder submitOrder) {
    	return questionRepository.save(submitOrder);
    }
    
}