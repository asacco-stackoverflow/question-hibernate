package com.stackoverflow.question.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.stackoverflow.question.entity.SubmitOrder;

@Repository
public interface QuestionRepository extends PagingAndSortingRepository<SubmitOrder, Long> {

}